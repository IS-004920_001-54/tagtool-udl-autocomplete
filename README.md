
# TagTool UDL + AutoComplete 
 
 - AutoCompletion and UDL for TagTool command scripts (\*.cmds)
 
 ***

## AutoComplete instructions

Move or copy `autoCompletion\TagTool.xml` to the `autoCompletion` folder in your Notepad++ install location.

Example location:

- `C:\Program Files (x86)\Notepad++\autoCompletion\TagTool.xml`

Restart Notepad++ if currently open.
  
#### NOTES:

Check "Enable auto-completion on each input" (Function and word completion) in `Settings` > `Preferences...` > `Auto-Completion` and change your value for "From Xth character" to whatever you want.
  
Autocomplete dropdown triggering is Case Sensitive. All entries from this autocomplete file begin with an **Uppercase** letter.
  
Autocomplete dropdown default shortcuts:

- `Ctrl+Space`
- `Ctrl+Enter`



***

## UDL instructions

Move or copy `userDefineLangs\TagTool.xml` to:

- `%AppData%\Roaming\Notepad++\userDefineLangs\TagTool.xml`
   
    

Restart Notepad++ if currently open.

#### NOTES:

UDL will be automatically applied to any .cmds file you open **after** installing this UDL.

***

Thanks to Enash for testing!